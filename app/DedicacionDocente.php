<?php

namespace FCS;

use Illuminate\Database\Eloquent\Model;

class DedicacionDocente extends Model
{
    protected $table = 'dedicacion_docente';
    protected $fillable=['id_profesor','id_periodo','h_docencia','por_docencia','h_investigacion','por_investigacion','h_extension','por_extension','h_administrativo','por_administrativo','h_otras','por_otras'];

    public function getProfesor()
    {
        return $this->belongsTo('\FCS\Profesor','id_profesor');
    } 
    
    public function getPeriodo()
    {
        return $this->belongsTo('\FCS\Periodo','id_periodo');
    } 

    public function getNombreProfesoresAttribute(){
        $profesores= Profesor::find($this->attributes["id_profesor"]);
        return $profesores->primer_nombre." ".$profesores->segundo_nombre." ".$profesores->primer_apellido." ".$profesores->segundo_apellido;
    }

    public function getNombrePeriodoAttribute(){
        $periodo= Periodo::find($this->attributes["id_periodo"]);
        return $periodo->anio." ".$periodo->periodo;
    }
}

    