<?php 
namespace FCS\Http\Requests\Profesor;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest {

	public function rules()
    {
        return [
                'cedula'            =>'required|unique:profesores,cedula|min:3|max:10',
                'primer_nombre'     =>'required|min:3|max:50',
                'segundo_nombre'    =>'min:3|max:50',
                'primer_apellido'   =>'required|min:3|max:50',
                'segundo_apellido'  =>'min:3|max:50',
                'email'             =>'required|email|unique:profesores,email',
                'telefono'          =>'min:7|max:10',                               
                ];
    }
    
    public function authorize()
    {
        return true;
    }
}