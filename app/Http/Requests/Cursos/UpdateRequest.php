<?php 
namespace FCS\Http\Requests\Cursos;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest {

    public function rules()
    {
        return [

                'nombre_curso'    =>    'required|min:4|max:50',
                'tipo_curso'      =>    'required|min:4|max:60',
                'semestre'        =>    'required|min:1|max:2',
                'area'            =>    'required|min:1|max:2',
                'creditos'        =>    'required|min:1',
                'id_planestudio'  =>    'required',
                ];
    }
    
    public function authorize()
    {
        return true;
    }

}