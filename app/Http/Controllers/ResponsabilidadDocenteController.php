<?php

namespace FCS\Http\Controllers;

use Illuminate\Http\Request;
use FCS\Http\Requests;
use FCS\Http\Controllers\Controller;

use FCS\Profesor;
use FCS\Grupo;
use FCS\Proyecto;
use FCS\Vinculacion;
use FCS\Periodo;
use FCS\Actividad;
use FCS\FuncionAdministrativa;
use FCS\ResponsabilidadDocente;
use FCS\ResponsabilidadAdministrativa;
use FCS\ResponsabilidadInvestigacion;
use FCS\ResponsabilidadOtras;
use FCS\ResponsabilidadProyeccion;
use Input;
use Excel;

use FCS\Http\Requests\RespDocen\CreateRequest;


use DB, View, Session, Redirect;
use Mockery\CountValidator\Exception;
use Psy\Exception\ErrorException;

class ResponsabilidadDocenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!\FCS\Permiso::verificarPermiso("responsabilidad-docente"))
        return redirect("/");
        $responsabilidad=ResponsabilidadDocente::All();
        $radmon = ResponsabilidadAdministrativa::All();
        $rotras = ResponsabilidadOtras::All();
        $reproy = ResponsabilidadProyeccion::All();
        $reinv  = ResponsabilidadInvestigacion::All();
        return view('componentes.responsabilidad_docente.index',compact('responsabilidad','radmon','rotras','reproy','reinv'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!\FCS\Permiso::verificarPermiso("responsabilidad-docente"))
        return redirect("/");
        $responsabilidad = new \FCS\ResponsabilidadDocente;
        $profesor        = Profesor::get()->lists("NombreProfesores","id");
        $funcion         = FuncionAdministrativa::allLists();
        $proyecto        = Proyecto::allLists();
        $grupo           = Grupo::get()->lists("NombreCompleto","id");
        $vinculacion     = Vinculacion::allLists();
        $actividad       = Actividad::allLists();
        $periodo         = Periodo::get()->lists("NombrePeriodo","id"); 
        $route           = [ 'route' => 'responsabilidad-docente.store'];
        return view('componentes.responsabilidad_docente.add',compact('route','responsabilidad','profesor','grupo','vinculacion','periodo','funcion','actividad','proyecto'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if(!\FCS\Permiso::verificarPermiso("responsabilidad-docente"))
        return redirect("/");

        $resdoc = new ResponsabilidadDocente;
            $resdoc->fill($request->all());
            $idprofesor = $request->get("id_profesor");
            $idgrupo    = $request->get("id_grupo");
            $idvinculacion = $request->get("id_vinculacion");
            $numestudiantes = $request->get("numero_estudiantes");
            $hdirectas = $request->get("horas_directas");
            $htutoria = $request->get("horas_tutoria");
            $hpreparacion = $request->get("horas_preparacion");
            $nsemanas = $request->get("numero_semanas");
            $total = (($hdirectas+$htutoria+$hpreparacion)*$nsemanas);
        
            $resdoc->id_profesor=$idprofesor;
            $resdoc->id_grupo=$idgrupo;
            $resdoc->id_vinculacion=$idvinculacion;
            $resdoc->numero_estudiantes=$numestudiantes;
            $resdoc->horas_directas=$hdirectas;
            $resdoc->horas_tutoria=$htutoria;
            $resdoc->horas_preparacion=$hpreparacion;
            $resdoc->numero_semanas=$nsemanas;
            $resdoc->total_horas=$total;
            $resdoc->save();

        $resadmin = new ResponsabilidadAdministrativa;
            $idperiodo = $request->get("id_periodo");
            $idfuncion = $request->get("id_funcion");
            $hadmin = $request->get("horas_admin");

            $resadmin->id_profesor=$idprofesor;
            $resadmin->id_vinculacion=$idvinculacion;
            $resadmin->id_periodo=$idperiodo;
            $resadmin->id_funcion=$idfuncion;
            $resadmin->horas_semanal=$hadmin;
            $resadmin->save();

        $resinv = new ResponsabilidadInvestigacion;
            $hinv = $request->get("horasi_semanal");
            $idproyectoi = $request->get("id_proyectoinv");

            $resinv->id_profesor=$idprofesor;
            $resinv->id_vinculacion=$idvinculacion;
            $resinv->id_periodo=$idperiodo;
            $resinv->id_proyecto=$idproyectoi;
            $resinv->horas_semanal=$hinv;
            $resinv->save();
        
        $respro = new ResponsabilidadProyeccion;
            $hpro = $request->get("horasp_semanal");
            $idproyectop = $request->get("id_proyectopro");

            $respro->id_profesor=$idprofesor;
            $respro->id_vinculacion=$idvinculacion;
            $respro->id_periodo=$idperiodo;
            $respro->id_proyecto=$idproyectop;
            $respro->horas_semanal=$hpro;
            $respro->save();

        $resotras = new ResponsabilidadOtras;
            $hotras = $request->get("horaso_semanal");
            $idactividad = $request->get("id_actividad");
            $fechainicio = $request->get("fecha_inicio");
            $fechatermina = $request->get("fecha_terminacion");
            $fechaentrega = $request->get("fecha_entregainforme");

            $resotras->id_profesor=$idprofesor;
            $resotras->id_vinculacion=$idvinculacion;
            $resotras->id_periodo=$idperiodo;
            $resotras->id_actividad=$idactividad;
            $resotras->horas_semanal=$hotras;
            $resotras->fecha_inicio=$fechainicio;
            $resotras->fecha_terminacion=$fechatermina;
            $resotras->fecha_entregainforme=$fechaentrega;
            $resotras->save();
            

        Session::flash('message','Responsabilidad Creadas Correctamente');
        return redirect::to('responsabilidad-docente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!\FCS\Permiso::verificarPermiso("responsabilidad-docente"))
        return redirect("/");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!\FCS\Permiso::verificarPermiso("responsabilidad-docente"))
        return redirect("/");
    
        $responsabilidad = ResponsabilidadDocente::find($id);
        $radmon = ResponsabilidadAdministrativa::find($id);
        $rotras = ResponsabilidadOtras::find($id);
        $reproy = ResponsabilidadProyeccion::find($id);
        $reinv = ResponsabilidadInvestigacion::find($id);
        $profesor        = Profesor::get()->lists("NombreProfesores","id");
        $funcion         = FuncionAdministrativa::allLists();
        $proyecto        = Proyecto::allLists();
        $grupo           = Grupo::get()->lists("NombreCompleto","id");
        $vinculacion     = Vinculacion::allLists();
        $actividad       = Actividad::allLists();
        $periodo         = Periodo::get()->lists("NombrePeriodo","id"); 
        $route = ['route'=>['responsabilidad-docente.update',$responsabilidad->id],'method'=>'PUT'];
        return view('componentes.responsabilidad_docente.edit',compact('route','responsabilidad','profesor','grupo',
            'vinculacion','periodo','funcion','proyecto','actividad','radmon','rotras','reproy','reinv'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateRequest $request, $id)
    {
        if(!\FCS\Permiso::verificarPermiso("responsabilidad-docente"))
        return redirect("/");

        $res=\FCS\ResponsabilidadDocente::find($id);
        $res->fill($request->all());
        $hdirectas=$request->get("horas_directas");
        $htutoria=$request->get("horas_tutoria");
        $hpreparacion=$request->get("horas_preparacion");
        $nsemanas=$request->get("numero_semanas");
        $total = (($hdirectas+$htutoria+$hpreparacion)*$nsemanas);
        $res->total_horas=$total;
        $res->save();

        Session::flash('message','Responsabilidad Docente Editada Correctamente');
        return redirect::to('responsabilidad-docente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!\FCS\Permiso::verificarPermiso("responsabilidad-docente"))
        return redirect("/");
        ResponsabilidadDocente::destroy($id);
        Session::flash('message','Responsabilidad Docente Eliminada Correctamente');
        return Redirect::to('/responsabilidad-docente');
    }

    public function descargarFormato($formato){
        $file=URL("formatos/".$formato.".xlsx");
        return redirect::to($file);
    }

    public function importar(){
        $destinationPath = 'upload';
        $fileName = Input::get("tipo").".xlsx";
        try{
            Input::file('archivo')->move($destinationPath, $fileName);
            Excel::load($destinationPath."/".$fileName, function($reader) {
                $reader->each(function($row){
                    $row=$row->toArray();
                    try {
                        if (Input::get("tipo") == "Academicas") {
                            $r = new ResponsabilidadDocente;
                            $docente = Profesor::where("cedula", "=", str_replace(".", "", $row['cedula']))->first();
                            $grupo = Grupo::select("grupos.id")->where("nombre_curso", "=", $row['curso'])->join("cursos", "cursos.id", "=", "grupos.id_curso")->first();
                            $vinculacion = Vinculacion::where("nombre_vinculacion", "=", DB::raw("'" . $row["tipo_vinculacion"] . "'"))->first();
                            $r->id_profesor = $docente->id;
                            $r->id_grupo = $grupo->id;
                            $r->id_vinculacion = $vinculacion->id;
                            $r->numero_estudiantes = $row['de_estudiantes'];
                            $r->horas_directas = $row['horas_directas'];
                            $r->horas_tutoria = $row['horas_de_tutoria'];
                            $r->horas_preparacion = $row['horas_de_preparacion'];
                            $r->numero_semanas = $row['de_semanas'];
                            $r->total_horas = $row['total_horas'];
                            $r->save();
                        } elseif (Input::get("tipo") == "Administrativas") {
                            $docente = Profesor::where("cedula", "=", str_replace(".", "", $row['cedula']))->first();
                            $periodo = Periodo::where("anio", "=", substr($row['periodo'], 0, 4))->where("periodo", "=", substr($row['periodo'], 4))->first();
                            $vinculacion = Vinculacion::where("nombre_vinculacion", "=", DB::raw("'" . $row["tipo_vinculacion"] . "'"))->first();
                            $funcion = FuncionAdministrativa::where("nombre_funcion", "=", DB::raw("'" . $row["funcion_administrativa"] . "'"))->first();
                            $resadmin = new ResponsabilidadAdministrativa;
                            $resadmin->id_profesor = $docente->id;
                            $resadmin->id_vinculacion = $vinculacion->id;
                            $resadmin->id_periodo = $periodo->id;
                            $resadmin->id_funcion = $funcion->id;
                            $resadmin->horas_semanal = $row["de_horas"];
                            $resadmin->save();
                        } elseif (Input::get("tipo") == "Proyeccion") {
                            $resinv = new ResponsabilidadProyeccion();
                            $docente = Profesor::where("cedula", "=", str_replace(".", "", $row['cedula']))->first();
                            $periodo = Periodo::where("anio", "=", substr($row['periodo'], 0, 4))->where("periodo", "=", substr($row['periodo'], 4))->first();
                            $vinculacion = Vinculacion::where("nombre_vinculacion", "=", DB::raw("'" . $row["tipo_vinculacion"] . "'"))->first();
                            $proyecto = Proyecto::where("nombre_proyecto", "=", DB::raw("'" . $row["proyecto"] . "'"))->first();
                            $resinv->id_profesor = $docente->id;
                            $resinv->id_vinculacion = $vinculacion->id;
                            $resinv->id_periodo = $periodo->id;
                            $resinv->id_proyecto = $proyecto->id;
                            $resinv->horas_semanal = $row["de_horas"];
                            $resinv->save();
                        } elseif (Input::get("tipo") == "Investigacion") {
                            $resinv = new ResponsabilidadInvestigacion();
                            $docente = Profesor::where("cedula", "=", str_replace(".", "", $row['cedula']))->first();
                            $periodo = Periodo::where("anio", "=", substr($row['periodo'], 0, 4))->where("periodo", "=", substr($row['periodo'], 4))->first();
                            $vinculacion = Vinculacion::where("nombre_vinculacion", "=", DB::raw("'" . $row["tipo_vinculacion"] . "'"))->first();
                            $proyecto = Proyecto::where("nombre_proyecto", "=", DB::raw("'" . $row["proyecto"] . "'"))->first();
                            $resinv->id_profesor = $docente->id;
                            $resinv->id_vinculacion = $vinculacion->id;
                            $resinv->id_periodo = $periodo->id;
                            $resinv->id_proyecto = $proyecto->id;
                            $resinv->horas_semanal = $row["de_horas"];
                            $resinv->save();
                        } else {
                            $resotras = new ResponsabilidadOtras;
                            $docente = Profesor::where("cedula", "=", str_replace(".", "", $row['cedula']))->first();
                            $periodo = Periodo::where("anio", "=", substr($row['periodo'], 0, 4))->where("periodo", "=", substr($row['periodo'], 4))->first();
                            $vinculacion = Vinculacion::where("nombre_vinculacion", "=", DB::raw("'" . $row["tipo_vinculacion"] . "'"))->first();
                            $actividad = Actividad::where("nombre_actividad", "=", DB::raw("'" . $row["actividad"] . "'"))->first();
                            $resotras->id_profesor = $docente->id;
                            $resotras->id_vinculacion = $vinculacion->id;
                            $resotras->id_periodo = $periodo->id;
                            $resotras->id_actividad = $actividad->id;
                            $resotras->horas_semanal = $row["de_horas"];
                            $resotras->fecha_inicio = $row["fecha_inicio"];
                            $resotras->fecha_terminacion = $row["fecha_termina"];
                            $resotras->fecha_entregainforme = $row["entrega_informe"];
                            $resotras->save();
                        }
                    }catch(Exception $e){

                    }catch(\PhpSpec\Exception\Example\ErrorException $e){

                    }
                });
            })->toArray();
            Session::flash('message', 'Importado correctamente!');
            return Redirect::to('responsabilidad-docente');
        }catch (Exception $e){
            Session::flash('message-error', 'Error al Importar, revise el archivo con los datos');
            return Redirect::to('responsabilidad-docente');
        }catch(ErrorException $e){
            Session::flash('message-error', 'Error al Importar, revise el archivo con los datos');
            return Redirect::to('responsabilidad-docente');
        }catch(\ErrorException $e){
            Session::flash('message-error', 'Error al Importar, revise el archivo con los datos');
            return Redirect::to('responsabilidad-docente');
        }
    }

}