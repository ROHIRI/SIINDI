<?php

namespace FCS\Http\Controllers;

use Illuminate\Http\Request;

use FCS\DedicacionDocente;
use FCS\Profesor;
use FCS\Periodo;

use FCS\Http\Requests;
use FCS\Http\Controllers\Controller;

use DB, View, Session, Redirect;

class DedicacionDocenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!\FCS\Permiso::verificarPermiso("dedicacion-docente"))
        return redirect("/");

        $dedicacion=DedicacionDocente::All();
        return view('componentes.dedicacion_docente.index',compact('dedicacion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!\FCS\Permiso::verificarPermiso("dedicacion-docente"))
        return redirect("/");
        $dedicacion= new \FCS\DedicacionDocente;
        $profesor = Profesor::get()->lists("NombreProfesores","id");
        $periodo = Periodo::get()->lists("NombrePeriodo","id");
        $route = [ 'route' => 'dedicacion-docente.store'];
        return view('componentes.dedicacion_docente.add',compact('route','dedicacion','profesor','periodo'));
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!\FCS\Permiso::verificarPermiso("dedicacion-docente"))
        return redirect("/");
        $dedicacion=new DedicacionDocente;
        $dedicacion->fill($request->all());
        
        $porcentaje = 100;
        $hdocencia=$request->get("h_docencia");
        $hinvestigacion=$request->get("h_investigacion");
        $hextension=$request->get("h_extension");
        $hadministrativo=$request->get("h_administrativo");
        $hotras=$request->get("h_otras");
        $totalhoras = ($hdocencia+$hinvestigacion+$hextension+$hadministrativo+$hotras);

        $dedicacion->por_docencia = number_format((($hdocencia*$porcentaje)/$totalhoras), 2, '.', '');
        $dedicacion->por_investigacion = number_format((($hinvestigacion*$porcentaje)/$totalhoras), 2, '.', ''); 
        $dedicacion->por_extension = number_format((($hextension*$porcentaje)/$totalhoras), 2, '.', '');
        $dedicacion->por_administrativo = number_format((($hadministrativo*$porcentaje)/$totalhoras), 2, '.', ''); 
        $dedicacion->por_otras = number_format((($hotras*$porcentaje)/$totalhoras), 2, '.', ''); 
        
        $dedicacion->save();
        return redirect('dedicacion-docente')->with('message','Dedicación Docente Creada Exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!\FCS\Permiso::verificarPermiso("dedicacion-docente"))
        return redirect("/");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!\FCS\Permiso::verificarPermiso("dedicacion-docente"))
        return redirect("/");

        $dedicacion= DedicacionDocente::find($id);
        $profesor = Profesor::get()->lists("NombreProfesores","id");
        $periodo = Periodo::get()->lists("NombrePeriodo","id"); 
        $route = [ 'route'=>['dedicacion-docente.update',$dedicacion->id],'method'=>'PUT'];
        return view('componentes.dedicacion_docente.edit',compact('route','dedicacion','profesor','periodo'));
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!\FCS\Permiso::verificarPermiso("dedicacion-docente"))
        return redirect("/");

        $dedicacion=\FCS\DedicacionDocente::find($id);
        $dedicacion->fill($request->all());
        
        $porcentaje = 100;
        $hdocencia=$request->get("h_docencia");
        $hinvestigacion=$request->get("h_investigacion");
        $hextension=$request->get("h_extension");
        $hadministrativo=$request->get("h_administrativo");
        $hotras=$request->get("h_otras");
        $totalhoras = ($hdocencia+$hinvestigacion+$hextension+$hadministrativo+$hotras);

        $dedicacion->por_docencia = number_format((($hdocencia*$porcentaje)/$totalhoras), 2, '.', '');
        $dedicacion->por_investigacion = number_format((($hinvestigacion*$porcentaje)/$totalhoras), 2, '.', ''); 
        $dedicacion->por_extension = number_format((($hextension*$porcentaje)/$totalhoras), 2, '.', '');
        $dedicacion->por_administrativo = number_format((($hadministrativo*$porcentaje)/$totalhoras), 2, '.', ''); 
        $dedicacion->por_otras = number_format((($hotras*$porcentaje)/$totalhoras), 2, '.', ''); 
        
        $dedicacion->save();

        Session::flash('message','Dedicacion Docente Editada Correctamente');
        return redirect::to('dedicacion-docente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!\FCS\Permiso::verificarPermiso("dedicacion-docente"))
        return redirect("/");
        DedicacionDocente::destroy($id);
        Session::flash('message','Dedicación Docente Eliminada Correctamente');
        return Redirect::to('/dedicacion-docente');
    }
}
