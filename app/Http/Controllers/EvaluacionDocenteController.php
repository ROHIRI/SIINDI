<?php

namespace FCS\Http\Controllers;

use Illuminate\Http\Request;
use FCS\Http\Requests;
use FCS\Http\Controllers\Controller;

use FCS\EvaluacionDocente;
use FCS\Profesor;
use FCS\Periodo;
use FCS\Http\Requests\EvaDocente\CreateRequest;
use DB, View, Session, Redirect;

class EvaluacionDocenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!\FCS\Permiso::verificarPermiso("evaluacion-docente"))
        return redirect("/");
        $evaluacion=EvaluacionDocente::All();
        return view('componentes.evaluacion_docente.index',compact('evaluacion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!\FCS\Permiso::verificarPermiso("evaluacion-docente"))
        return redirect("/");
        $evaluacion= new \FCS\EvaluacionDocente;
        $profesor = Profesor::get()->lists("NombreProfesores","id");
        $periodo = Periodo::get()->lists("NombrePeriodo","id");
        $route = [ 'route' => 'evaluacion-docente.store'];
        return view('componentes.evaluacion_docente.addevaluaciondocente',compact('route','evaluacion','profesor','periodo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if(!\FCS\Permiso::verificarPermiso("evaluacion-docente"))
        return redirect("/");
        $eva=new EvaluacionDocente;
        $eva->fill($request->all());
        $valor=$request->get("valor");
        if($valor>=0 && $valor<55){
            $eva->nota="Insuficiente";
        }elseif($valor>=55 && $valor<70){
            $eva->nota="Aceptable";
        }
        elseif($valor>=70 && $valor<90){
            $eva->nota="Sobresaliente";
        }else{
            $eva->nota="Excelente";
        }
        $eva->save();
        return redirect('evaluacion-docente')->with('message','Evaluación Docente Creada Exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!\FCS\Permiso::verificarPermiso("evaluacion-docente"))
        return redirect("/");
        $evaluacion= EvaluacionDocente::find($id);
        $profesor = Profesor::get()->lists("NombreProfesores","id");
        $periodo = Periodo::get()->lists("NombrePeriodo","id");
        $route = [ 'route'=>['evaluacion-docente.update',$evaluacion->id],'method'=>'PUT'];
        return view('componentes.evaluacion_docente.editevaluacion', compact('evaluacion','route','profesor','periodo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateRequest $request, $id)
    {
        if(!\FCS\Permiso::verificarPermiso("evaluacion-docente"))
        return redirect("/");
        
        $eva=\FCS\EvaluacionDocente::find($id);
        $eva->fill($request->all());
        $valor=$request->get("valor");
        if($valor>=0 && $valor<55){
            $eva->nota="Insuficiente";
        }elseif($valor>=55 && $valor<70){
            $eva->nota="Aceptable";
        }
        elseif($valor>=70 && $valor<90){
            $eva->nota="Sobresaliente";
        }else{
            $eva->nota="Excelente";
        }
        $eva->save();
        
        Session::flash('message','Evaluación Docente Editada Correctamente');
        return redirect::to('evaluacion-docente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!\FCS\Permiso::verificarPermiso("evaluacion-docente"))
        return redirect("/");
        EvaluacionDocente::destroy($id);
        Session::flash('message','Evaluación Docente Eliminada Correctamente');
        return Redirect::to('/evaluacion-docente');
    }
}