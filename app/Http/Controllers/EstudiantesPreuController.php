<?php

namespace FCS\Http\Controllers;

use FCS\EstudiantesPreu;
use FCS\Periodo;

use Illuminate\Http\Request;
use FCS\Http\Requests;
use FCS\Http\Controllers\Controller;
use FCS\Http\Requests\Estudiante\CreateRequest;
use FCS\Http\Requests\Estudiante\UpdateRequest;
use DB, View, Session, Redirect;


class EstudiantesPreuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!\FCS\Permiso::verificarPermiso("secretaria-estudiante"))
        return redirect("/");

        $estudiantes=EstudiantesPreu::All();
        return view('componentes.Estudiantes_preu.index',compact('estudiantes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!\FCS\Permiso::verificarPermiso("secretaria-estudiante"))
        return redirect("/");
        $estudiante= new \FCS\EstudiantesPreu;
        $periodo = Periodo::get()->lists("NombrePeriodo","id");
        $route = [ 'route' => 'secretaria-estudiante.store'];
        return view('componentes.Estudiantes_preu.agregarpreu',compact('route','estudiante','periodo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        if(!\FCS\Permiso::verificarPermiso("secretaria-estudiante"))
        return redirect("/");
        EstudiantesPreu::create($request->all());
        return redirect('secretaria-estudiante')->with('message','Datos Estudiantes Por Periodo Creado Exitosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!\FCS\Permiso::verificarPermiso("secretaria-estudiante"))
        return redirect("/");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!\FCS\Permiso::verificarPermiso("secretaria-estudiante"))
        return redirect("/");
        
        $estudiantes=EstudiantesPreu::find($id);
        $periodo = Periodo::get()->lists("NombrePeriodo","id");
        $route = [ 'route'=>['secretaria-estudiante.update',$estudiantes->id],'method'=>'PUT'];
        return view('componentes.Estudiantes_preu.edit',compact('route','estudiantes','periodo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        if(!\FCS\Permiso::verificarPermiso("secretaria-estudiante"))
        return redirect("/");

        $estudiantes=EstudiantesPreu::find($id);
        $periodo = Periodo::get()->lists("NombrePeriodo","id");
        $estudiantes->fill($request->all());
        $estudiantes->save();

        Session::flash('message','Datos Estudiantes por Periodo Editado Correctamente');
        return redirect::to('/secretaria-estudiante');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!\FCS\Permiso::verificarPermiso("secretaria-estudiante"))
        return redirect("/");

        EstudiantesPreu::destroy($id);
        Session::flash('message','Datos Estudiantes Por Periodo Eliminado Correctamente');
        return Redirect::to('secretaria-estudiante');
    }
}