<?php

namespace FCS;

use Illuminate\Database\Eloquent\Model;

class ResponsabilidadProyeccion extends Model
{
    protected $table = 'responsabilidades_proyeccion';
    protected $fillable=[ 'id_profesor',
    					  'id_vinculacion',
    					  'id_periodo',
    					  'id_proyecto',
    					  'horas_semanal'
    					  ]; 
    public function getPeriodoCompletoAttribute(){
        $periodo=Periodo::find($this->attributes["id_periodo"]);
        return $periodo->anio." ".$periodo->periodo;
    }
    public function getProfesor()
    {
        return $this->belongsTo('\FCS\Profesor','id_profesor');
    } 
    
    public function getPeriodo()
    {
        return $this->belongsTo('\FCS\Periodo','id_periodo');
    } 

    public function getProyecto()
    {
    	return $this->belongsTo('\FCS\Proyecto','id_proyecto');
    }
    
    public function getNombreVinculacionAttribute(){
        $vinculacion=Vinculacion::find($this->attributes["id_vinculacion"]);
        return $vinculacion->nombre_vinculacion;
    }

    public function getNombreProfesoresAttribute(){
        $profesores=Profesor::find($this->attributes["id_profesor"]);
        return $profesores->primer_nombre." ".$profesores->segundo_nombre." ".$profesores->primer_apellido." ".$profesores->segundo_apellido;
    }
}
