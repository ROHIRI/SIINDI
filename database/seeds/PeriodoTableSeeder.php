<?php

use Illuminate\Database\Seeder;

class PeriodoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FCS\Periodo::create([
            'anio'=>'2015',
            'periodo'=>'I'
            ]);
        FCS\Periodo::create([
            'anio'=>'2015',
            'periodo'=>'II'
            ]);
        FCS\Periodo::create([
            'anio'=>'2016',
            'periodo'=>'I'
            ]);
        FCS\Periodo::create([
            'anio'=>'2016',
            'periodo'=>'II'
            ]);
    }
}
