<?php

use Illuminate\Database\Seeder;

class ProyectosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FCS\Proyecto::create([
            'codigo'          => '1753',
            'nombre_proyecto' => 'FACTORES RELACIONADOS CON LA ADQUISICION DE MEDICAMENTOS EN LAS DROGUERIAS',
            'tipo'            => 'Investigacion'
            ]);
        FCS\Proyecto::create([
            'codigo'          => '1754',
            'nombre_proyecto' => 'CONOCIMIENTOS DE LOS PROFESIONALES DE ENFERMERÍA EN EL USO DE TECNOLOGÍA AVANZADA',
            'tipo'            => 'Investigacion'
            ]);
        FCS\Proyecto::create([
            'codigo'          => '1812',
            'nombre_proyecto' => 'TALLER  PARA CUIDADORES INFORMALES',
            'tipo'            => 'Proyeccion'
            ]);
        FCS\Proyecto::create([
            'codigo'          => '1755',
            'nombre_proyecto' => 'CONOCIMIENTO SOBRE DENGUE QUE TIENEN LOS HABITANTES DE LA REGION DEL ARIARI',
            'tipo'            => 'Investigacion'
            ]);
        FCS\Proyecto::create([
            'codigo'          => '1756',
            'nombre_proyecto' => 'LOS CONOCIMIENTOS Y LAS ACTITUDES QUE SOBRE EL CUIDADO DE LA SALUD DE LOS COLECTIVOS TIENEN LOS PROFESIONALES',
            'tipo'            => 'Investigacion'
            ]);
        FCS\Proyecto::create([
            'codigo'          => '1884',
            'nombre_proyecto' => 'Diseño e implementacion de un programa para fortalecer pautas de crianza en adolescentes gestantes',
            'tipo'            => 'Proyeccion'
            ]);
        FCS\Proyecto::create([
            'codigo'          => '1695',
            'nombre_proyecto' => 'SOCIALIZACION FAMILIAR PERCIBIDA EN LOS ESTUDIANTES DEL MEGACOLEGIO RODOLFO LLINAS DE LA COMUNIDAD DEL 13 DE MAYO',
            'tipo'            => 'Investigacion'
            ]);
        FCS\Proyecto::create([
            'codigo'          => '1808',
            'nombre_proyecto' => 'Zonas de Orientación Universitaria ZOU',
            'tipo'            => 'Investigacion'
            ]);
        FCS\Proyecto::create([
            'codigo'          => '1757',
            'nombre_proyecto' => 'CARACTERISTICAS SOCIODEMOGRAFICAS CONDICION Y PERCEPCION DE LA SALUD Y RIESGOS OCUPACIONALES',
            'tipo'            => 'Investigacion'
            ]);
        FCS\Proyecto::create([
            'codigo'          => '1809',
            'nombre_proyecto' => 'Implementacion de esquema de ejercicio fisico para aliviar la carga de la enfermedad',
            'tipo'            => 'Proyeccion'
            ]);
    }
}

