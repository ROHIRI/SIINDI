<?php

use Illuminate\Database\Seeder;


class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FCS\User::create([
            'name'=>'william',
            'email'=>'william@gmail.com',
            'dependencia'=>'Secretaria',
            'password'=> bcrypt('admin')
            ]);
    }
}
