<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FuncionesAdministrativas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funciones_administrativas', function(Blueprint $table){
            $table->increments('id');
            $table->string('nombre_funcion',250);
            $table->string('sesion_consejo',100)->nullable();
            $table->date('fecha_sesion')->nullable();
            $table->float('max_horas')->nullable();
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('funciones_administrativas');
    }
}
