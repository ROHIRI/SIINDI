<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Auditorias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tabla')->unsigned();
            $table->string('modulo',255);
            $table->string('accion',255);
            $table->text('datos');
            $table->integer('usuario')->unsigned();
            $table->date('fecha');
            $table->text('datos_anteriores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('auditorias');
    }
}
