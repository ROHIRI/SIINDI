<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Movilidades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movilidades', function(Blueprint $table){
            $table->increments('id');
            $table->enum('tipo_movilidad',['Profesor','Estudiante']);
            $table->integer('id_profesor')->unsigned()->nullable();
            $table->integer('id_estudiante')->unsigned()->nullable();
            $table->integer('id_evento')->unsigned();
            $table->integer('id_periodo')->unsigned();
            $table->foreign('id_profesor')
                  ->references('id')
                  ->on('profesores')
                  ->onUpdate('CASCADE');
            $table->foreign('id_estudiante')
                  ->references('id')
                  ->on('estudiantes')
                  ->onUpdate('CASCADE');
            $table->foreign('id_evento')
                  ->references('id')
                  ->on('eventos')
                  ->onUpdate('CASCADE');
            $table->foreign('id_periodo')
                  ->references('id')
                  ->on('periodos')
                  ->onUpdate('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('movilidades');
    }
}
