<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Eventos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('eventos', function(Blueprint $table){
            $table->increments('id');
            $table->string('numero_consejo',100)->nullable();
            $table->date('fecha')->nullable();
            $table->string('nombre_evento',250);
            $table->string('descripcion_evento',250)->nullable();
            $table->string('lugar',250)->nullable();
            $table->enum('caracter_evento',['Nacional','Internacional']);
            $table->integer('id_tipoeventos')->unsigned();
            $table->foreign('id_tipoeventos')
                  ->references('id')
                  ->on('tipo_eventos')
                  ->onUpdate('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('eventos');
    }
}
