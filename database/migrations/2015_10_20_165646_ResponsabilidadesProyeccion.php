<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResponsabilidadesProyeccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responsabilidades_proyeccion', function(Blueprint $table){
            $table->increments('id');
            $table->integer('id_profesor')->unsigned();
            $table->integer('id_vinculacion')->unsigned();
            $table->integer('id_periodo')->unsigned();
            $table->integer('id_proyecto')->unsigned();
            $table->float('horas_semanal')->nullable();
            $table->foreign('id_profesor')
                  ->references('id')
                  ->on('profesores')
                  ->onUpdate('CASCADE');
            $table->foreign('id_vinculacion')
                  ->references('id')
                  ->on('vinculaciones')
                  ->onUpdate('CASCADE');
            $table->foreign('id_periodo')
                  ->references('id')
                  ->on('periodos')
                  ->onUpdate('CASCADE');
            $table->foreign('id_proyecto')
                  ->references('id')
                  ->on('proyecto')
                  ->onUpdate('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('responsabilidades_proyeccion');
    }
}
