@extends('layaouts.tablas')
@section('content')
  <section class="content"> 
    @include('componentes.responsabilidad_docente.partials.modal')
      <div class="modal modal-default" id="modalImportar">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">Importar Responsabilidades Docente</h4>
                  </div>
                  {!! Form::open(array("url"=>array("responsabilidad-docente/importar"),"method"=>"POST","files"=>"true")) !!}
                      <div class="modal-body">
                          <div class="row">
                              <div class="col-xs-12">
                                  <div class="well"><b>Descargar formato para importación:</b><br><a href="{!! URL('descargar_formato/academica') !!}" target="_blank">Academicas</a>&nbsp;&nbsp;&nbsp;<a href="{!! URL('descargar_formato/administrativa') !!}" target="_blank">Administrativas</a>&nbsp;&nbsp;&nbsp;<a href="{!! URL('descargar_formato/investigacion') !!}" target="_blank">Investigacion</a>&nbsp;&nbsp;&nbsp;<a href="{!! URL('descargar_formato/proyeccion') !!}" target="_blank">Proyeccion</a>&nbsp;&nbsp;&nbsp;<a href="{!! URL('descargar_formato/otras') !!}" target="_blank">Otras</a></div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-12">
                                  {!!Form::label('Tipo de Responsabilidades a importar')!!}<br>
                                  {!!Form::select('tipo', array("Academicas"=>"Academicas","Administrativas"=>"Administrativas","Investigacion"=>"Investigacion","Otras"=>"Otras","Proyeccion"=>"Proyeccion"), null,['class'=>'form-control select2', 'placeholder'=>'Seleccione El Tipo','required'])!!} <br>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-12">
                                  {!!Form::label('Archivo a importar')!!}<br>
                                  {!! Form::file('archivo') !!}
                              </div>
                          </div>
                      </div>
                      <div class="modal-footer">
                          <button type="submit" class="btn btn-success">Importar</button>
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                      </div>
                  {!! Form::close() !!}
              </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <div class="row form-group">
          <div class="col-xs-12">
              <button type="button" class="btn btn-success" onclick="$('#modalImportar').modal('show');"><i class="fa fa-file"></i> Importar</button>
          </div>
      </div>
    <div class="row">
      <div class="col-xs-12">
        @include('layaouts.partials.mensaje')
        <div class="box box-danger">
          <div class="box-header with-border">
    
            <h3 class="box-title">Listado de Responsabilidad Docente</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
          </div><!-- /.box-header -->
          <div class="box-body">
          <!--
            <div class="row form-group">
              <div class="col-md-3">
                <a href="{!! URL('responsabilidad-docente/create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Nueva Responsabilidad</a>
              </div>
            </div>
            -->
            <table id="example3" class="table table-bordered table-striped">
              <thead>
                <!--<th>Sm</th> Lo Saco de Curso --> 
                <th>Profesor</th>
                <th>Codigo</th>
                <th>Curso</th>
                <th>Vinculacion</th>
                <th>P.A</th>
                <th>N.E</th>
                <th>H.D</th>
                <th>H.T</th>
                <th>H.PP</th>
                <th>N.S</th>
                <th>T.H</th>
                </thead>
              <tbody>
                @foreach($responsabilidad as $t)
                  <tr> 
                    <td>{{ $t->NombreProfesores}}</td>
                    <td>{{ $t->getGrupo->NombreCodigo}}</td>
                    <td>{{ $t->getGrupo->NombreCurso}}</td>
                    <td>{{ $t->NombreVinculacion}}</td>
                    <td>{{ $t->getGrupo->PeriodoCompleto}}</td>
                    <td>{{ $t->numero_estudiantes}}</td>
                    <td>{{ $t->horas_directas}}</td>
                    <td>{{ $t->horas_tutoria}}</td>
                    <td>{{ $t->horas_preparacion}}</td>
                    <td>{{ $t->numero_semanas}}</td>
                    <td>{{ $t->total_horas}}</td>                     
                  </tr>
                @endforeach
              </tbody>
            </table>   
          </div><!-- /.box-body -->
        </div><!-- /.box -->
        <div class="box box-danger">
          <div class="box-header with-border">
    
            <h3 class="box-title">Listado de Responsabilidad Administrativa</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
          </div><!-- /.box-header -->
          <div class="box-body">
          <!--
            <div class="row form-group">
              <div class="col-md-3">
                <a href="{!! URL('responsabilidad-docente/create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Nueva Responsabilidad</a>
              </div>
            </div>
            -->
            <table id="example4" class="table table-bordered table-striped">
              <thead>
                <!--<th>Sm</th> Lo Saco de Curso --> 
                <th>Profesor</th>
                <th>Vinculacion</th>
                <th>Periodo</th>
                <th>Función</th>
                <th>H.S</th>
                
              </thead>
              <tbody>
                @foreach($radmon as $t)
                  <tr> 
                    <td>{{ $t->NombreProfesores}}</td>
                    <td>{{ $t->NombreVinculacion}}</td>
                    <td>{{ $t->PeriodoCompleto}}</td>
                    <td>{{ $t->getFuncion->nombre_funcion}}</td>
                    <td>{{ $t->horas_semanal}}</td>             
                  </tr>
                @endforeach
              </tbody>
            </table>   
          </div><!-- /.box-body -->
        </div><!-- /.box -->
        <div class="box box-danger">
          <div class="box-header with-border">
    
            <h3 class="box-title">Listado de Responsabilidad Investigacion</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <!--
            <div class="row form-group">
              <div class="col-md-3">
                <a href="{!! URL('responsabilidad-docente/create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Nueva Responsabilidad</a>
              </div>
            </div>
            -->
            <table id="example5" class="table table-bordered table-striped">
              <thead>
                <th>Profesor</th>
                <th>Vinculacion</th>
                <th>Periodo</th>
                <th>Proyecto</th>
                <th>Codigo</th>
                <th>H.S</th>
                <th>Fecha Inicio</th>
                <th>Fecha Terminación</th>
                <th>Sesión del Consejo</th>
                <th>Fecha de Sesión</th>
                
              </thead>
              <tbody>
                @foreach($reinv as $t)
                  <tr> 
                    <td>{{ $t->NombreProfesores}}</td>
                    <td>{{ $t->NombreVinculacion}}</td>
                    <td>{{ $t->PeriodoCompleto}}</td>
                    <td>{{ $t->getProyecto->nombre_proyecto}}</td>
                    <td>{{ $t->getProyecto->codigo}}</td>
                    <td>{{ $t->horas_semanal}}</td>
                    <td>{{ $t->getProyecto->fecha_inicio}}</td>
                    <td>{{ $t->getProyecto->fecha_terminacion}}</td>
                    <td>{{ $t->getProyecto->sesion_consejo}}</td>
                    <td>{{ $t->getProyecto->fecha_sesion}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>   
          </div><!-- /.box-body -->
        </div><!-- /.box -->
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Listado de Responsabilidad Proyección</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
          </div><!-- /.box-header -->
          <div class="box-body">
            <!--
            <div class="row form-group">
              <div class="col-md-3">
                <a href="{!! URL('responsabilidad-docente/create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Nueva Responsabilidad</a>
              </div>
            </div>
            -->
            <table id="example6" class="table table-bordered table-striped">
              <thead>
                <th>Profesor</th>
                <th>Vinculacion</th>
                <th>Periodo</th>
                <th>Proyecto</th>
                <th>Codigo</th>
                <th>H.S</th>
                <th>Fecha Inicio</th>
                <th>Fecha Terminación</th>
                <th>Sesión del Consejo</th>
                <th>Fecha de Sesión</th>
                
              </thead>
              <tbody>
                @foreach($reproy as $t)
                  <tr> 
                    <td>{{ $t->NombreProfesores}}</td>
                    <td>{{ $t->NombreVinculacion}}</td>
                    <td>{{ $t->PeriodoCompleto}}</td>
                    <td>{{ $t->getProyecto->nombre_proyecto}}</td>
                    <td>{{ $t->getProyecto->codigo}}</td>
                    <td>{{ $t->horas_semanal}}</td>
                    <td>{{ $t->getProyecto->fecha_inicio}}</td>
                    <td>{{ $t->getProyecto->fecha_terminacion}}</td>
                    <td>{{ $t->getProyecto->sesion_consejo}}</td>
                    <td>{{ $t->getProyecto->fecha_sesion}}</td>                   
                    
                  </tr>
                @endforeach
              </tbody>
            </table>   
          </div><!-- /.box-body -->
        </div><!-- /.box -->
        <div class="box box-danger">
          <div class="box-header with-border">
    
            <h3 class="box-title">Listado de Otras Responsabilidades</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  </div>
          </div><!-- /.box-header -->
          <div class="box-body">
          <!--
            <div class="row form-group">
              <div class="col-md-3">
                <a href="{!! URL('responsabilidad-docente/create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Nueva Responsabilidad</a>
              </div>
            </div>
            -->
            <table id="example7" class="table table-bordered table-striped">
              <thead>
                <!--<th>Sm</th> Lo Saco de Curso --> 
                <th>Profesor</th>
                <th>Vinculacion</th>
                <th>Periodo</th>
                <th>Actividad</th>
                <th>Horas Semana</th>
                <th>Fecha Inicio</th>
                <th>Fecha Terminación</th>
                <th>Fecha de Entrega Informe</th>
                
              </thead>
              <tbody>
                @foreach($rotras as $t)
                  <tr> 
                    <td>{{ $t->NombreProfesores}}</td>
                    <td>{{ $t->NombreVinculacion}}</td>
                    <td>{{ $t->PeriodoCompleto}}</td>
                    <td>{{ $t->getActividad->nombre_actividad}}</td>
                    <td>{{ $t->horas_semanal}}</td>
                    <td>{{ $t->fecha_inicio}}</td>
                    <td>{{ $t->fecha_terminacion}}</td>
                    <td>{{ $t->fecha_entregainforme}}</td>
                    
                  </tr>
                @endforeach
              </tbody>
            </table>   
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row -->                
  </section><!-- /.content -->
@endsection
@section('js-adicional')
  <script type="text/javascript">
    $(function () {
                $('#example4').DataTable({
                    language: {
                        lengthMenu:        "Mostrar _MENU_ Registros por Pagina",
                        search:            "Buscar ",
                        searchPlaceholder: "Busca por columnas",
                        info:              "Mostrar _START_ A _END_ De _TOTAL_ Registros",
                        zeroRecords:       "Ningun Registro Encontrado",
                        infoEmpty:         "No Hay Registros Disponibles",
                    paginate: {
                        first:         "Primero",
                        previous:      "<< Anterior",
                        next:          "Siguiente >>",
                        last:          "Ultimo"
                        }
                    }
                });
                $('#example5').DataTable({
                    language: {
                        lengthMenu:        "Mostrar _MENU_ Registros por Pagina",
                        search:            "Buscar ",
                        searchPlaceholder: "Busca por columnas",
                        info:              "Mostrar _START_ A _END_ De _TOTAL_ Registros",
                        zeroRecords:       "Ningun Registro Encontrado",
                        infoEmpty:         "No Hay Registros Disponibles",
                    paginate: {
                        first:         "Primero",
                        previous:      "<< Anterior",
                        next:          "Siguiente >>",
                        last:          "Ultimo"
                        }
                    }
                });
                $('#example6').DataTable({
                    language: {
                        lengthMenu:        "Mostrar _MENU_ Registros por Pagina",
                        search:            "Buscar ",
                        searchPlaceholder: "Busca por columnas",
                        info:              "Mostrar _START_ A _END_ De _TOTAL_ Registros",
                        zeroRecords:       "Ningun Registro Encontrado",
                        infoEmpty:         "No Hay Registros Disponibles",
                    paginate: {
                        first:         "Primero",
                        previous:      "<< Anterior",
                        next:          "Siguiente >>",
                        last:          "Ultimo"
                        }
                    }
                });
                $('#example7').DataTable({
                    language: {
                        lengthMenu:        "Mostrar _MENU_ Registros por Pagina",
                        search:            "Buscar ",
                        searchPlaceholder: "Busca por columnas",
                        info:              "Mostrar _START_ A _END_ De _TOTAL_ Registros",
                        zeroRecords:       "Ningun Registro Encontrado",
                        infoEmpty:         "No Hay Registros Disponibles",
                    paginate: {
                        first:         "Primero",
                        previous:      "<< Anterior",
                        next:          "Siguiente >>",
                        last:          "Ultimo"
                        }
                    }
                });
            });
  </script>
@stop