@extends('layaouts.tablas')
@section('content')
 <section class="content">
    <div class="row">
      <div class="col-md-12"> 
      @include('errors.partials.requesterror')
        <div class="box box-solid box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Agregar Responsabilidades Docentes</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="pad">
                <div class="nav-tabs-custom">
                  <!-- Tabs within a box -->
                  <ul class="nav nav-tabs pull-right">
                    <li class="tab1 tab-title"><a href="#tab1" data-toggle="tab">Otras R.</a></li>
                    <li class="tab2 tab-title"><a href="#tab2" data-toggle="tab">Proyeccion</a></li>
                    <li class="tab3 tab-title"><a href="#tab3" data-toggle="tab">Investigacion</a></li>
                    <li class="tab4 tab-title"><a href="#tab4" data-toggle="tab">Administrativas</a></li>
                    <li class="active tab5 tab-title"><a href="#tab5" data-toggle="tab">Docencia</a></li>
                    <li class="pull-left header"><i class="fa fa-plus-circle"></i>Tipos de Responsabilidades</li>
                  </ul>
                  {!! Form::open($route) !!}
                  <div class="tab-content no-padding">
                    <div class="tab-pane active" id="tab5">
                      @include('componentes.responsabilidad_docente.partials.form')   
                    </div>
                    <div class="tab-pane active" id="tab4">
                      @include('componentes.responsabilidad_docente.partials.formResAdministrativas')   
                    </div>
                    <div class="tab-pane active" id="tab3">
                      @include('componentes.responsabilidad_docente.partials.formResInvestigacion')  
                    </div>
                    <div class="tab-pane active" id="tab2">
                      @include('componentes.responsabilidad_docente.partials.formResProyeccion')   
                    </div>
                    <div class="tab-pane active" id="tab1">
                      @include('componentes.responsabilidad_docente.partials.formResOtras') 
                    </div>
                  </div>                  
                  {!!Form::close()!!}
                </div><!-- /.nav-tabs-custom -->    
              </div>
            </div><!-- /.row pad-->
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div>
    </div>
  </section><!-- /.content -->
@endsection
@section('js-adicional')
  <script type="text/javascript">
    function calcularMaximo(){
        var maximo=0;
        switch($("#id_vinculacion").val()){
          case "1"://Planta tiempo completo
            maximo=40;
          break;
          case "2"://Planta medio tiempo
            maximo=20;
          break;
          case "3"://Ocasional tiempo completo
            maximo=40;
          break;
          case "4"://Ocasional medio tiempo
            maximo=20;
          break;
          case "5"://Catedratico
            maximo=20;
          break;
          default:
            maximo=0;
        }
        return maximo;
    }
    $(document).ready(function(){
      $(".horas").change(function(){
        var horas = $(".horas");
        var totalHoras=0;
        var maximo=calcularMaximo();
        if(maximo==0){
          swal("Debes Seleccionar Una Vinculacion Primero!", "Click en el Boton Para Volver a Intentar", "error");
          $("#id_vinculacion").select2("open");
          $(this).val("0");
          return;
        }
        horas.each(function(){
         if($(this).val()!="")
          totalHoras+=parseInt($(this).val());
        });
        if(totalHoras>maximo){
          $(this).val("0");
          swal("Excediste El Numero de Horas", "Click en el Boton Para Volver a Intentar", "error");
        }
      });
      $("#id_vinculacion").change(function(){
        var horas = $(".horas");
        var totalHoras=0;
        var maximo=calcularMaximo();
        horas.each(function(){
         if($(this).val()!="")
          totalHoras+=parseInt($(this).val());
        });
        if(totalHoras>maximo){
          swal("Excediste El Numero de Horas", "Click en el Boton Para Volver a Intentar", "error");      
          horas.each(function(){
            $(this).val("0");
          });
        }
      });
    });
    function validar(){
      var valid=true;
      $(".required").each(function(){
        if($(this).val()==""){
          $(".tab-pane").removeClass("active");
          $(".tab-title").removeClass("active");
          $("."+$(this).parent().parent().attr("id")).addClass("active");
          $(this).parent().parent().addClass("active");
          if(/select2/.test($(this).attr("class")))
            $(this).select2("open");
          else
            $(this).focus();
          valid=false;
          return false;
        }
      });
      if(valid)
        $("form").submit();
    }
  </script>
@stop