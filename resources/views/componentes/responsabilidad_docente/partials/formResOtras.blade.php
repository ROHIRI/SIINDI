<div class="form-group">                           
    {!!Form::label('Actividad')!!}<br>
    {!!Form::select('id_actividad', $actividad, null,['class'=>'form-control select2', 'placeholder'=>'Seleccione la Actividad','required'])!!} <br>    
    
    {!!Form::label('Horas Semanales')!!}<br>
    {!!Form::Number('horaso_semanal',null,['class'=>'form-control horas','placeholder'=>'Ingrese la cantidad de horas','min'=>'0','required'])!!}

    {!!Form::label('Fecha de Inicio')!!}<br>
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
        {!!Form::input('date', 'fecha_inicio', null, ['class' => 'form-control','required']);!!}
    </div>

    {!!Form::label('Fecha de Terminacion')!!}<br>
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
        {!!Form::input('date', 'fecha_terminacion', null, ['class' => 'form-control','required']);!!}
    </div>

    {!!Form::label('Fecha de Entrega Informe')!!}<br>
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
        {!!Form::input('date', 'fecha_entregainforme', null, ['class' => 'form-control','required']);!!}
    </div>
</div>
<div class="form-group">
    {!!Form::button('Guardar',['class'=>'btn btn-primary','onclick'=>'validar();'])!!}
</div>


