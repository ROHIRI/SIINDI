<div class="form-group">                           
    {!!Form::label('Funcion Directiva')!!}<br>
    {!!Form::Select('id_funcion', $funcion, null,['class'=>'form-control select2', 'placeholder'=>'Seleccione la Funcion del Profesor','required'])!!} <br>

    {!!Form::label('Periodo')!!}<br>
    {!!Form::select('id_periodo', $periodo, null, ['class'=>'form-control select2','placeholder' => 'Seleccion el Periodo','required'])!!} <br>

    {!!Form::label('Horas Semanales')!!}<br>
    {!!Form::number('horas_admin', null,['class'=>'form-control horas', 'placeholder'=>'Digite las Horas Administrativas del Profesor','required', 'min:1'])!!} <br>     
</div>
