<div class="form-group">
    {!!Form::label('Profesor')!!}<br>
    {!!Form::select('id_profesor', $profesor, null, ['class'=>'form-control select2','placeholder' => '','required'])!!} <br>

    {!!Form::label('Periodo')!!}<br>
    {!!Form::select('id_periodo', $periodo, null, ['class'=>'form-control select2','placeholder' => '','required'])!!} <br>

    {!!Form::label('Valor')!!}<br>
    
    {!!Form::Number ('valor',null,['class'=>'form-control','placeholder'=>'Ingrese el Valor de la Evaluacion','required','min=0','max=100'])!!}
</div>