@extends('layaouts.tablas')
@section('content')
  <section class="content">  
    @include('componentes.planes_de_estudio.partials.modal')
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              @include('layaouts.partials.mensaje')
              <h3 class="box-title">Listado Planes Estudio</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row form-group">
                <div class="col-md-3">
                  <a href="{!! URL('planes-estudio/create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Nuevo Plan Estudio</a>
                </div>
              </div>
            <table id="example3" class="table table-bordered table-striped">
              <thead>
                <th>Codigo</th>
                <th>Fecha Resolución</th>
                <th>Numero Resolución</th>
                <th>Fecha Inicio</th>
                <th>Fecha Fin</th>
                <th>Programa</th>
                <th>Acciones</th>
              </thead>
              <tbody>
                @foreach($plan as $t)
                  <tr>
                    <td>{{ $t->codigo}}</td>
                    <td>{{ $t->fecha_resolucion}}</td>
                    <td>{{ $t->numero_resolucion}}</td>
                    <td>{{ $t->fecha_inicio}}</td>
                    <td>{{ $t->fecha_fin}}</td>
                    <td>{{ $t->programa->nombre_programa}}</td>
                    <td>
                      {!! link_to_route('planes-estudio.edit', $title='Editar', $parameters=$t->id, $atrributes=['class'=>'btn btn-warning']) !!}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>   
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div>                  
  </section><!-- /.content -->  
@endsection