<div class="form-group">
    {!!Form::label('Periodo')!!}<br>
    {!!Form::select('id_periodo', $periodo, null, ['class'=>'form-control select2','placeholder' => 'Seleccion el Periodo','required'])!!} <br>

    {!!Form::label('Cantidad de Estudiantes Inscritos')!!}
    {!!Form::Number('e_inscritos',null,['class'=>'form-control','placeholder'=>'Ingrese La Cantidad de Estudiantes Inscritos','required'])!!}

    {!!Form::label('Cantidad de Estudiantes Admitidos')!!}
    {!!Form::Number('e_admintidos',null,['class'=>'form-control','placeholder'=>'Ingrese La Cantidad de Estudiantes admitidos','required'])!!}

    {!!Form::label('Cantidad de Estudiantes Matriculados')!!}
    {!!Form::Number('matriculados_total',null,['class'=>'form-control','placeholder'=>'Ingrese La Cantidad de Estudiantes  Matriculados','required'])!!}

    {!!Form::label('Cantidad de Estudiantes Matriculados Primer Semestre')!!}
    {!!Form::Number('matriculados_primersemestre',null,['class'=>'form-control','placeholder'=>'Ingrese La Cantidad de Estudiantes Matriculados Primer Semestre','required'])!!}
                            
    {!!Form::label('Cantidad de Estudiantes Graduados')!!}
    {!!Form::Number('graduados',null,['class'=>'form-control','placeholder'=>'Ingrese La Cantidad de Estudiantes Graduados','required'])!!}
    
    {!!Form::label('Cantidad de Estudiantes Retirados')!!}
    {!!Form::Number('retirados',null,['class'=>'form-control','placeholder'=>'Ingrese La Cantidad de Estudiantes Retirados','required'])!!}
    
    {!!Form::label('Tasa Deserción Estudiantil')!!}
    {!!Form::Number('tasa_desercion',null,['class'=>'form-control','placeholder'=>'Ingrese La Tasa De Desercion Infantil','required'])!!}

    {!!Form::label('Tasa de Culminación Estudiantil')!!}
    {!!Form::Number('tasa_culminacion',null,['class'=>'form-control','placeholder'=>'Ingrese La Tasa De Culminación Estudiantil','required'])!!}
</div>