<div class="form-group">
    
    {!!Form::label('Nombre del Curso')!!}
    {!!Form::Text('nombre_curso',null,['class'=>'form-control','placeholder'=>'Ingrese el Nombre del Curso','required'])!!}

    {!!Form::label('Tipo del Curso')!!}
    {!!Form::select('tipo_curso', array('Teórico' => 'Teórico','Practico' =>'Practico','Teórico-Practico'=>'Teórico-Practico'), null ,['class'=>'form-control select2','placeholder'=>'','required'])!!} 

    {!!Form::label('Semestre')!!}
    {!!Form::select('semestre', array(
        'I'    => 'Primer Semestre',
        'II'   => 'Segundo Semestre',
        'III'  => 'Tercer Semestre',
        'IV'   => 'Cuarto Semestre',
        'V'    => 'Quinto Semestre',
        'VI'   => 'Sexto Semestre',
        'VII'  => 'Septimo Semestre',
        'VIII' => 'Octavo Semestre',
        'IX'   => 'Noveno Semestre',
        'X'    => 'Decimo Semestre')
        ,null,['class'=>'form-control select2','placeholder'=>'','required'])!!}<br>

    {!!Form::label('Área')!!}
    {!!Form::select('area', array(
        'Pz'   => 'Profundización',
        'Ps'   => 'Profesional',
        'Cm'   => 'Complementaria'
    ),null,['class'=>'form-control select2','placeholder'=>'','required'])!!}<br>

    {!!Form::label('Cantidad de Créditos')!!}
    {!!Form::select('creditos', array(
        '2'   => '2',
        '3'   => '3',
        '4'   => '4'
    ),null,['class'=>'form-control select2','placeholder'=>'','required'])!!}<br>
                           
    {!!Form::label('Planes de Estudio')!!}<br>
    {!!Form::select('id_planestudio', $planes, null, ['class'=>'form-control select2','placeholder'=>'','required'])!!} <br>
</div>