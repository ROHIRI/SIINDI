@extends('layaouts.tablas')
@section('content')
  <section class="content"> 
    @include('componentes.dedicacion_docente.partials.modal')  
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            @include('layaouts.partials.mensaje')
            <h3 class="box-title">Listado Dedicación Docente</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <div class="row form-group">
              <div class="col-md-3">
                <a href="{!! URL('dedicacion-docente/create') !!}" class="btn btn-success"><i class="fa fa-plus"></i> Nueva Dedicación</a>
              </div>
            </div>
            <table id="example3" class="table table-bordered table-striped">
              <thead>
                <th>Profesor</th>
                <th>Periodo</th>
                <th>H. D</th>
                <th>%</th>
                <th>H. I</th>
                <th>%</th>
                <th>H. E</th>
                <th>%</th>
                <th>H. A</th>
                <th>%</th>
                <th>H. O</th>
                <th>%</th>
                <th>Acciones</th>
              </thead>
              <tbody>
                @foreach($dedicacion as $d)
                  <tr> 
                    <td>{{$d->NombreProfesores}}</td>
                    <td>{{$d->NombrePeriodo}}</td>
                    <td>{{$d->h_docencia}}</td>
                    <td>{{$d->por_docencia.""."%"}}</td>
                    <td>{{$d->h_investigacion}}</td>
                    <td>{{$d->por_investigacion.""."%" }}</td>
                    <td>{{$d->h_extension }}</td>
                    <td>{{$d->por_extension.""."%" }}</td>
                    <td>{{$d->h_administrativo }}</td>
                    <td>{{$d->por_administrativo.""."%" }}</td>
                    <td>{{$d->h_otras }}</td>
                    <td>{{$d->por_otras.""."%" }}</td>
                    <td>
                      {!! link_to_route('dedicacion-docente.edit', $title='Editar', $parameters=$d->id, $atrributes=['class'=>'btn btn-warning']) !!}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            <div class="row">
              <div class="col-xs-12">
                <ul>
                  <li>HD= Horas Docencia</li>
                  <li>HD= Horas Investigación</li>
                  <li>HD= Horas Extensión</li>
                  <li>HD= Horas Administrativo</li>
                  <li>HO= Otras Horas</li>
                </ul>
              </div>
            </div> 
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row -->                
  </section><!-- /.content -->
@endsection