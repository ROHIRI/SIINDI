@extends('layaouts.newtablas')
@section('content')

    <section class="content">
      <div class="row"> 
      <div class="col-lg-12">
      
          <div class="box">
            <div class="box-header" data-original-title>
              <h2><i class="fa fa-book"></i><span class="break"></span>DATOS GENERALES HOMOLOGACIÓN EXTERNA</h2>
              <div class="box-icon">
              
                <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                
              </div>
            </div>

            <div class="box-content">
              <form class="form-horizontal">
                  {!!Form::open() !!}  
                  <div class="form-group">
                  
            {!! Form::label('Oficina de admisiones, registro y control')!!}
            {!!Form::Text('requistro_control',null,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}
                  
            {!! Form::label('Director de programa')!!}
            {!!Form::Text('director_programa',null,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}
                  
             {!! Form::label('Fecha')!!} 
            {!!Form::text('fecha',null,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}
             
               

            {!! Form::label('Homologación del estudiante')!!}
            {!!Form::Text('anio',null,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}

            {!!Form::label('Codigo del estudiante')!!}<br>
            {!!Form::text('semestre',null,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}

            {!!Form::label('Universidad origen')!!}<br>
            {!!Form::text('semestre',null,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}

            {!!Form::label('Programa origen')!!}<br>
            {!!Form::text('semestre',null,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}

            {!!Form::label('Universidad destino')!!}<br>
            {!!Form::text('semestre',null,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}

            {!!Form::label('Programa destino')!!}<br>
            {!!Form::text('semestre',null,['class'=>'controls','class'=>'form-control focused','for'=>'focusedInput'])!!}

                   
           
                  </div>
                 


            <div class="box-header" data-original-title>
              <h2><i class="fa fa-book"></i><span class="break"></span>DATOS UNIVERSIDAD ORIGEN</h2>
              <div class="box-icon">
                
                <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                
              </div>
            </div>
            <div class="box-content">
              <table class="table table-striped table-bordered bootstrap-datatable datatable" id="tablahe1">
                <thead>
                  <tr>
                    <th>Semestre</th>
                    <th>Cursos</th>
                    <th>Código</th>
                    <th>Créditos</th>
                    <th>Nota</th>
                    
                  </tr>
                </thead>   
                <tbody>
                <tr>
                
                  
                  
                  <td class="center"></td>
                  <td class="center"></td>
                  <td class="center"></td>
                  <td class="center"></td>
                  <td class="center"></td>
                  
                </tr>
                
                
              </table> 
              
              
                  
                         
            </div>
          
                   
              <div class="box-header" data-original-title>
              <h2><i class="fa fa-book"></i><span class="break"></span>DATOS UNIVERSIDAD DE LOS LLANOS</h2>
              <div class="box-icon">
                
                <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                
              </div>
            </div>
            
              <table class="table table-striped table-bordered bootstrap-datatable datatable" id="tablahe2">
                <thead>
                  <tr>
                    <th>Semestre</th>
                    <th>Cursos</th>
                    <th>Código</th>
                    <th>Créditos</th>
                    <th>Nota</th>
                    
                  </tr>
                </thead>   
                <tbody>
                <tr>
                
                  
                  
                  <td class="center"></td>
                  <td class="center"></td>
                  <td class="center"></td>
                  <td class="center"></td>
                  <td class="center"></td>
                  
                </tr>
                
                
              </table> 
              
             <div class="form-group">
            {!!Form::submit('Guardar',['class'=>'btn btn-primary'])!!}
                  </div>
                         
            </div>
         


         
              
          
                  {!! Form::close() !!}
                         
            </div>      
          
          
        
      
      </div><!--/row-->
    </div>

  </section><!-- /.content -->
  
@endsection
@section('js-adicional')
    <script>
        $(document).ready(function(){
            var tabla = $('#tablahe1').editableTableWidget();
            var tabla = $('#tablahe2').editableTableWidget();
        }); 
    </script>
@stop