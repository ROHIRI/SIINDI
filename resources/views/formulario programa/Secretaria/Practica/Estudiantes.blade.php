@extends('layaouts.newtablas')    
@section('content')
<section class="content">
  <div class="row"> 
    <div class="col-md-12">
      <div class="box">
        <div class="box-header" data-original-title>
          <h2><i class="fa fa-stethoscope"></i><span class="break"></span><b>Selección del Grupo a los Estudiantes</b></h2>
          <div class="box-icon">
            <a href="#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
          </div>
        </div>
        <div class="box-content"> 
          {!! Form::open($route) !!}
            <div class="row form-group">
              <table class="table table-responsive">
                <thead>
                  <tr>
                    <th>Estudiante</th>
                    <th>Grupo</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($estudiantes as $e)
                    <?php
                      $g=\FCS\Grupos_practica::where("id_estudiante","=",\DB::raw($e->id))->where("id_practica","=",\DB::raw($id))->first();
                      $valor="";
                      if(!is_null($g))
                        $valor=$g->grupo;
                    ?>
                    <tr>
                      <td>{!! $e->codigo_estudiante." - ".$e->apellido_paterno." ".$e->apellido_materno." ".$e->primer_nombre." ".$e->segundo_nombre !!}</td>
                      <td>{!! Form::text("grupo".$e->id,$valor,["class"=>"form-control"]) !!}</td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <div class="row form-group">
              <div class="col-md-12">
                {!!Form::submit('Guardar',['class'=>'btn btn-success'])!!}
              </div>     
            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</section>




@endsection