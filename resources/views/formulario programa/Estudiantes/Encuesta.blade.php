@extends('layaouts.newtablas')
@section('content')

<section class="content">
  {!! Form::open(array("url"=>URL("encuesta"),"method"=>"POST")) !!}
      <div class="row"> 
      <div class="col-lg-12">

  
      <!-- start: Content -->
     
  <div class="box">       
         
  <div class="box-header" data-original-title>
  <h2><i class="fa fa-pencil-square-o"></i><b>Evaluación de Curso por Parte de Estudiantes</b></h2>
  <div class="box-icon">
  </div>
  </div>


  <div class="box-content">
  <div id="MyWizard" class="wizard">
  <ul class="steps">

                    <li data-target="#step1" class="active"><span class="badge badge-info">1</span></li>
                    <li data-target="#step2"><span class="badge">2</span></li>
                    <li data-target="#step3"><span class="badge">3</span></li>
  </ul>

                  <div class="actions">
                    <button type="button" class="btn btn-prev"> <i class="fa fa-arrow-left"></i>Anterior</button>
                        <button type="button" class="btn btn-success btn-next" data-last="Finish">Siguiente<i class="fa fa-arrow-right"></i></button>
                  </div>
                </div>

                <div class="step-content">
                  <div class="step-pane active" id="step1">
                    <div class="box-content">
                      <table class="table bootstrap-datatable datatable">
                          <thead>
                            <tr>
                              <th style="width: 60%">Preguntas</th>
                              <th style="width: 10%">Siempre</th>
                              <th style="width: 10%">Casi siempre</th>
                              <th style="width: 10%">Algunas veces</th>                                       
                              <th style="width: 10%">Nunca</th>                                       
                            </tr>
                          </thead>   
                          <tbody>
                            @foreach($preguntas as $p)
                              <tr>
                                <td>{!! $p->pregunta !!}</td>
                                <td class="center">
                                <input type="radio" name="question{!! $p->idPregunta !!}" value="1">
                                </td>
                                <td class="center">
                                <input type="radio" name="question{!! $p->idPregunta !!}" value="2">
                                </td>
                                <td class="center">
                                <input type="radio" name="question{!! $p->idPregunta !!}" value="3">                         
                                </td>
                                <td class="center">
                                <input type="radio" name="question{!! $p->idPregunta !!}" value="4">                         
                                </td>   
                              </tr>                    
                            @endforeach           
                          </tbody>
                       </table>
                     </div>
                  </div>
                  <div class="step-pane" id="step2">
                    <div class="box-content">
                      <label class="control-label" for="observaciones"><b>Observaciones al Curos:</b></label>
                      <div class="controls">
                        <textarea id="observaciones" name="observaciones"
                                  rows="0"
                                  style="width: 100%;height: 150px;resize: none;"
                                  maxlength="500">
                              </textarea>
                      </div>
                    </div>                    
                  </div>
                  
                  <div class="step-pane" id="step3">
                    <div class="box-content">
                      <label class="control-label" for="observaciones"><b>Sugerencias al Curso</b></label>
                      <div class="controls">
                        <textarea id="sugerencias" name="sugerencias"
                                  rows="0"
                                  style="width: 100%;height: 150px;resize: none;"
                                  maxlength="500">
                              </textarea>
                      </div>
                      <div>
                        <button type="submit" class="btn btn-success"></i> Guardar</button>
                      </div>
                    </div>                    
                  </div>
                </div>
              </div>
            </div>          
          </div><!--/col-->
        </div><!--/row-->
      </div>

      </div><!--/row-->
      </div>
        <input type="hidden" name="grupo" value="{!! $grupo !!}">
      {!! Form::close() !!}
      </section>
      <!-- e

      <!-- end: Content -->       
   
  @endsection